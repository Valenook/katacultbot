import time

import aiohttp
import json


class Message:
    """message for chatbase"""
    def __init__(self,
                 platform='',
                 message='',
                 intent='',
                 version='',
                 user_id='',
                 type=None,
                 not_handled=False,
                 time_stamp=int(round(time.time() * 1e3))):

        self.api_key = None
        self.platform = platform
        self.message = message
        self.intent = intent
        self.version = version
        self.user_id = user_id
        self.not_handled = not_handled
        self.time_stamp = time_stamp
        if not type and type != 'user' and type != 'agent':
            raise ValueError('Type must be user or agent')
        else:
            self.type = type

    def to_json(self):
        return json.dumps(self, default=lambda i: i.__dict__)


class ChatbaseSender:
    """chatbase reporter"""
    def __init__(self, auth_key):
        self.auth_key = auth_key

    async def send_message(self, message: Message):
        message.api_key = self.auth_key
        async with aiohttp.ClientSession() as session:
            headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
            data = message.to_json()
            async with session.post('https://chatbase.com/api/message',
                                    data=data,
                                    headers=headers) as response:
                response = await response.json()
                return response
