#from aiocache import RedisCache
from aiocache.serializers import StringSerializer, PickleSerializer, JsonSerializer


class CacheDriverError(Exception):
    def __init__(self, default_message='Unknown Error in CacheDriver'):
        super().__init__(default_message)


class CacheDriver:
    def __init__(self, caches, serializer=PickleSerializer):
        self.caches = caches
        self.serializer = serializer
        self.cache = None

    def set_config(self, config, cache_system='default'):
        """"""
        print(config)
        if config['redis_alt']:
            config['redis_alt']['plugins'] = [
             {'class': "aiocache.plugins.HitMissRatioPlugin"},
             {'class': "aiocache.plugins.TimingPlugin"}
        ]
        self.caches.set_config(config)

    def cache_create(self, cache_system):
        """"""
        self.cache = self.caches.create(cache_system)

    async def get_cache(self, key):
        """"""
        if self.cache:
            cache_aux = await self.cache.get(key)
            return cache_aux if cache_aux else {}
        else:
            raise ValueError('Cache not created')

    async def set_cache(self, key, value):
        """"""
        if self.cache:
            return await self.cache.set(key, value)
        else:
            raise ValueError('Cache not created')

    async def update_dict(self, key, dict_value):
        if self.cache:
            if self.serializer == PickleSerializer or self.serializer == JsonSerializer:
                """"""
                cache_dict = await self.cache.get(key)
                for key_j in dict_value.keys():
                    for key_jj in dict_value[key_j].keys():
                        cache_dict[key_j].update({key_jj: dict_value[key_j][key_jj]})
                await self.set_cache(key, cache_dict)
            else:
                raise CacheDriverError('This method allow only for PickleSerializer or JsonSerializer')
        else:
            raise ValueError('Cache not created')

    async def clear_cache(self, key, cache_system='default'):
        """"""
        await self.caches.get(cache_system).delete(key)
