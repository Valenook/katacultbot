from sqlalchemy import MetaData, Table, Column, String, Boolean, DateTime, ForeignKey, Integer

from config import config

metadata = MetaData()

artist = Table(
    "artist", metadata,
    Column("id", Integer, primary_key=True, unique=True),
    Column("name", String(config['database']['string_len']['name']), unique=True),
    Column("info_short", String(config['database']['string_len']['info_short'])),
    Column("info_long", String(config['database']['string_len']['info_long'])),
    Column("url_music_1", String(config['database']['string_len']['url_music_1'])),
    Column("url_music_2", String(config['database']['string_len']['url_music_2'])),
    Column("url_1_text", String(config['database']['string_len']['url_music_1_text'])),
    Column("url_2_text", String(config['database']['string_len']['url_music_2_text'])),
    Column("act_info", String(config['database']['string_len']['act_info'])),
    Column('date_id', Integer, ForeignKey('date_table.id')),
)

dancefloor = Table(
    "dancefloor", metadata,
    Column("id", Integer, primary_key=True, unique=True),
    Column("name", String(config['database']['string_len']['name']), unique=True),
)

schedule = Table(
    "schedule", metadata,
    Column("dancefloor_id", Integer, ForeignKey("dancefloor.id"), nullable=False),
    Column("artist_id", Integer, ForeignKey("artist.id"), nullable=False),
    Column("date_id", Integer, ForeignKey("date_table.id"), nullable=False),
)

date_table = Table(
    "date_table", metadata,
    Column("id", Integer, primary_key=True),
    Column("date", DateTime, nullable=False),
    Column("dancefloor_id", Integer, ForeignKey('dancefloor.id'), nullable=False),
    Column("is_busy", Boolean, nullable=False),
)

user = Table(
    "user", metadata,
    Column("id", Integer, primary_key=True),
    Column("tg_id", String(32), unique=True),
    Column("tg_chat_id", String(32), unique=True),
    Column("is_push_forbidden", Boolean),
)

notification = Table(
    "notification", metadata,
    Column("id", Integer, primary_key=True),
    Column("chat_id_raw", Integer, nullable=False),
    Column("date_raw", DateTime, nullable=False),
    Column("scene_raw", String(config['database']['string_len']['name']), nullable=False),
    Column("artist_raw", String(config['database']['string_len']['name']), nullable=False),
)

