import os
from glob import glob

from sqlalchemy import MetaData, Table, Column, String, Boolean, DateTime, ForeignKey, Integer
from config import config
from sqlalchemy import create_engine
import pandas as pd


DATABASE_URL = "{user}:{password}@{host}:5432/{name}".format(
    name=config["database"]["name"],
    user=config["database"]["user"],
    password=config["database"]["password"],
    host=config["database"]["host"]
)

engine = create_engine(f"postgresql://{DATABASE_URL}")
con = engine.connect()
trans = con.begin()

files = glob(f'{os.getcwd()}/*.csv')

for file in files:
    df = pd.read_csv(file, engine='python', encoding='utf-8')
    df = df.where((pd.notnull(df)), None)
    for index, row in df.iterrows():
        try:
            engine.execute("""INSERT INTO artist
                           (name, info_short, info_long, url_music_1, url_music_2, url_1_text, url_2_text, act_info)
                           VALUES (%s, %s, %s, %s, %s, %s, %s, %s);""",
                           (row["Artist name"],
                           row["Artist info"],
                           None,
                           row["Music link"],
                           None,
                           None,
                           None,
                           None))

            trans.commit()
        except Exception as e:
            print(e)
    con.close()
