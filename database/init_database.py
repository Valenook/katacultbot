from datetime import datetime, date, time

from sqlalchemy import create_engine

from database.models.tables import metadata, artist, dancefloor, schedule, date_table, notification, user

from config import config

DATABASE_URL = "{user}:{password}@{host}:5432/{name}".format(
    name=config["database"]["name"],
    user=config["database"]["user"],
    password=config["database"]["password"],
    host=config["database"]["host"]
)

engine = create_engine(f"postgresql://{DATABASE_URL}")

notification.create(engine, checkfirst=True)

metadata.create_all(engine)

#for dancefloorname in ['Angar', 'Cement', 'Depo', 'Topka', 'Garden', 'Tokar']:
#    engine.execute(dancefloor.insert(), name=dancefloorname)
for dancefloorname in ['Forma', 'Tseh', 'Topka',]:
    engine.execute(dancefloor.insert(), name=dancefloorname)

#date_dict = {24: {20, 21, 22, 23},
#             25: {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 23},
#             26: {0, 1}}
#date_dict = {25: {17, 18, 19, 20, 21, 22, }}
date_dict = {29: {21, 21, 22, 23},
             30: {0, 1, 2, 3, 4, 5, 6, 7, 8},}

#for dancefloorname in ['Angar', 'Cement', 'Depo', 'Topka', 'Garden', 'Tokar']:
for dancefloorname in ['Forma', 'Tseh', 'Topka',]:
    dancefloor_id = engine.execute(dancefloor.select().where(dancefloor.c.name == dancefloorname)).first()[0]
    for day in date_dict.keys():
        for hour in date_dict[day]:
            if dancefloorname in ['Forma', 'Tseh']:
                if day == 29 and hour in [22,] and dancefloorname == 'Forma':
                    date_combined = datetime.combine(date(2019, 11, day), time(hour, 15))
                    engine.execute(date_table.insert(), date=date_combined, is_busy=False,
                                   dancefloor_id=dancefloor_id)
                    date_combined = datetime.combine(date(2019, 11, day), time(hour, 45))
                    engine.execute(date_table.insert(), date=date_combined, is_busy=False,
                                   dancefloor_id=dancefloor_id)
                elif day == 30 and hour in [0] and dancefloorname == 'Forma':
                    date_combined = datetime.combine(date(2019, 11, day), time(hour, 45))
                    engine.execute(date_table.insert(), date=date_combined, is_busy=False,
                                   dancefloor_id=dancefloor_id)
                elif day == 30 and hour in [1] and dancefloorname == 'Forma':
                    date_combined = datetime.combine(date(2019, 11, day), time(hour, 50))
                    engine.execute(date_table.insert(), date=date_combined, is_busy=False,
                                   dancefloor_id=dancefloor_id)
                elif day == 30 and hour in [4] and dancefloorname == 'Forma':
                    date_combined = datetime.combine(date(2019, 11, day), time(hour, 15))
                    engine.execute(date_table.insert(), date=date_combined, is_busy=False,
                                   dancefloor_id=dancefloor_id)
                elif day == 30 and hour in [3, 5, 6, 7] and dancefloorname == 'Tseh':
                    date_combined = datetime.combine(date(2019, 11, day), time(hour, 45))
                    engine.execute(date_table.insert(), date=date_combined, is_busy=False,
                                   dancefloor_id=dancefloor_id)

            date_combined = datetime.combine(date(2019, 11, day), time(hour, 0))
            engine.execute(date_table.insert(), date=date_combined, is_busy=False, dancefloor_id=dancefloor_id)
            date_combined = datetime.combine(date(2019, 11, day), time(hour, 30))
            engine.execute(date_table.insert(), date=date_combined, is_busy=False, dancefloor_id=dancefloor_id)






