from aiopg.sa import create_engine

from config import config


DATABASE_URL = "dbname={name} user={user} password={password} host={host}".format(
    name=config["database"]["name"],
    user=config["database"]["user"],
    password=config["database"]["password"],
    host=config["database"]["host"]
)


async def connection_executor(operation):
    engine = await create_engine(DATABASE_URL)
    async with engine:
        async with engine.acquire() as connection:
            return await operation(connection)
