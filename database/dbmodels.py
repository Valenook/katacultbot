from sqlalchemy import and_, or_

from database.clients import connection_executor
from database.models import artist
from database.models import dancefloor
from database.models import schedule
from database.models import date_table
from database.models import user
from database.models import notification


class User:
    @staticmethod
    async def select(**kwargs):
        async def operation(connection):
            if kwargs.get('tg_id'):
                query = user.select() \
                    .where(user.c.tg_id == str(kwargs['tg_id']))
            elif kwargs.get('tg_chat_id'):
                query = user.select() \
                    .where(user.c.tg_chat_id == str(kwargs['tg_chat_id']))
            elif kwargs.get('id'):
                query = user.select() \
                    .where(user.c.id == int(kwargs['id']))
            async for row in connection.execute(query):
                return row

        return await connection_executor(operation)

    @staticmethod
    async def select_all():
        async def operation(connection):
            query = user.select()
            query_all = []
            async for row in connection.execute(query):
                query_all.append(row)
            return query_all
        return await connection_executor(operation)

    @staticmethod
    async def insert(data):
        async def operation(connection):
            query = user.insert() \
                .values(**data)
            await connection.execute(query)

        return await connection_executor(operation)

    @staticmethod
    async def update(tg_chat_id, data):
        async def operation(connection):
            query = user.update() \
                .where(user.c.tg_chat_id == str(tg_chat_id)) \
                .values(**data)
            await connection.execute(query)

        return await connection_executor(operation)

    @staticmethod
    async def delete(**kwargs):
        raise NotImplementedError


class Notification:
    @staticmethod
    async def select(**kwargs):
        async def operation(connection):
            if kwargs.get('chat_id_raw'):
                query = notification.select() \
                    .where(notification.c.chat_id_raw == int(kwargs['chat_id_raw']))
            async for row in connection.execute(query):
                return row
        return await connection_executor(operation)

    @staticmethod
    async def select_all():
        async def operation(connection):
            query = notification.select()
            query_all = []
            async for row in connection.execute(query):
                query_all.append(row)
            return query_all
        return await connection_executor(operation)

    @staticmethod
    async def insert(data):
        async def operation(connection):
            query = notification.insert() \
                .values(**data)
            await connection.execute(query)

        return await connection_executor(operation)

    @staticmethod
    async def update(id, data):
        raise NotImplementedError

    @staticmethod
    async def delete(**kwargs):
        async def operation(connection):
            if kwargs.get('id'):
                query = notification.delete() \
                    .where(notification.c.id == int(kwargs['id']))
            await connection.execute(query)
        return await connection_executor(operation)


class Artist:

    @staticmethod
    async def select(**kwargs):
        async def operation(connection):
            if kwargs.get('name'):
                query = artist.select() \
                    .where(artist.c.name == str(kwargs['name']))
                async for row in connection.execute(query):
                    return row

            elif kwargs.get('contains'):
                # query = artist.select() \
                #     .where(artist.c.name.contains(str(kwargs['contains'])))
                query = artist.select() \
                    .where(artist.c.name.ilike("%{}%".format(str(kwargs['contains']))))
                artists = []
                async for row in connection.execute(query):
                    artists.append(row)
                return artists

            elif kwargs.get('id'):
                query = artist.select() \
                    .where(artist.c.id == int(kwargs['id']))
                async for row in connection.execute(query):
                    return row

            elif kwargs.get('date_id'):
                query = artist.select() \
                    .where(artist.c.date_id == int(kwargs['date_id']))
                async for row in connection.execute(query):
                    return row

        return await connection_executor(operation)

    @staticmethod
    async def select_all(flag=True):
        """
        :param flag: show all in case True, show without date in case False
        """
        async def operation(connection):
            if flag == True:
                query = artist.select()
            elif flag == 'busy':
                query = artist.select(artist.c.date_id != None)
            else:
                query = artist.select().where(artist.c.date_id == None)
            query_all = []
            async for row in connection.execute(query):
                query_all.append(row)
            return query_all
        return await connection_executor(operation)

    @staticmethod
    async def select_list(ids_list):
        async def operation(connection):
            filters = []
            for i in ids_list:
                filters.append(artist.c.id == i)
            query = artist.select() \
                .where(or_(*filters))
            query_all = []
            async for row in connection.execute(query):
                query_all.append(row)
            return query_all

        return await connection_executor(operation)

    @staticmethod
    async def insert(data):
        async def operation(connection):
            query = artist.insert() \
                .values(**data)
            await connection.execute(query)

        return await connection_executor(operation)

    @staticmethod
    async def update(id, data):
        async def operation(connection):
            query = artist.update() \
                .where(artist.c.id == int(id)) \
                .values(**data)
            await connection.execute(query)

        return await connection_executor(operation)

    @staticmethod
    async def delete(**kwargs):
        async def operation(connection):
            if kwargs.get('id'):
                query = artist.delete() \
                    .where(artist.c.id == int(kwargs['id']))
            elif kwargs.get('name'):
                query = artist.delete() \
                    .where(artist.c.name == str(kwargs['name']))

            await connection.execute(query)

        return await connection_executor(operation)


class Dancefloor:

    @staticmethod
    async def select(id_):
        async def operation(connection):
            query = dancefloor.select() \
                .where(dancefloor.c.id == int(id_))
            async for row in connection.execute(query):
                return row

        return await connection_executor(operation)

    @staticmethod
    async def select_all():
        async def operation(connection):
            query = dancefloor.select()
            query_all = []
            async for row in connection.execute(query):
                query_all.append(row)
            return query_all
        return await connection_executor(operation)

    @staticmethod
    async def insert(data):
        async def operation(connection):
            query = dancefloor.insert() \
                .values(**data)
            await connection.execute(query)

        return await connection_executor(operation)

    @staticmethod
    async def update(name, data):
        async def operation(connection):
            query = dancefloor.update() \
                .where(dancefloor.c.id == str(name)) \
                .values(**data)
            await connection.execute(query)

        return await connection_executor(operation)

    @staticmethod
    async def delete(name):
        async def operation(connection):
            query = dancefloor.delete() \
                .where(dancefloor.c.id == str(name))
            await connection.execute(query)

        return await connection_executor(operation)


class Schedule:

    @staticmethod
    async def select(**kwargs):
        async def operation(connection):
            if kwargs.get('dancefloor_id'):
                query = schedule.select() \
                    .where(schedule.c.dancefloor_id == kwargs['dancefloor_id'])
            elif kwargs.get('artist_id'):
                query = schedule.select() \
                    .where(schedule.c.artist_id == kwargs['artist_id'])
            elif kwargs.get('date_id'):
                query = schedule.select() \
                    .where(schedule.c.date_id == kwargs['date_id'])
            async for row in connection.execute(query):
                return row

        return await connection_executor(operation)

    @staticmethod
    async def select_all(**kwargs):
        async def operation(connection):
            if kwargs.get('dancefloor_id'):
                query = schedule.select() \
                    .where(schedule.c.dancefloor_id == kwargs['dancefloor_id'])
            elif kwargs.get('artist_id'):
                query = schedule.select() \
                    .where(schedule.c.artist_id == kwargs['artist_id'])
            elif kwargs.get('date_id'):
                query = schedule.select() \
                    .where(schedule.c.date_id == kwargs['date_id'])
            query_all = []
            async for row in connection.execute(query):
                query_all.append(row)
            return query_all

        return await connection_executor(operation)

    @staticmethod
    async def insert(data):
        async def operation(connection):
            query = schedule.insert() \
                .values(**data)
            await connection.execute(query)

        return await connection_executor(operation)

    @staticmethod
    async def update(name, data):
        async def operation(connection):
            query = schedule.update() \
                .where(schedule.c.id == str(name)) \
                .values(**data)                             # @TODO CHANGE
            await connection.execute(query)

        return await connection_executor(operation)

    @staticmethod
    async def delete(**kwargs):
        async def operation(connection):
            if kwargs.get('artist_id'):
                query = schedule.delete() \
                    .where(schedule.c.artist_id == int(kwargs['artist_id']))
            elif kwargs.get('date_id'):
                query = schedule.delete() \
                    .where(schedule.c.date_id == int(kwargs['date_id']))
            await connection.execute(query)

        return await connection_executor(operation)


class DateTable:

    @staticmethod
    async def select(dancefloor_id):
        async def operation(connection):
            filters = [date_table.c.is_busy == False, date_table.c.dancefloor_id == dancefloor_id]
            query = date_table.select() \
                .where(and_(*filters))
            query_all = []
            async for row in connection.execute(query):
                query_all.append(row)
            return query_all

        return await connection_executor(operation)

    @staticmethod
    async def select_id(id_):
        async def operation(connection):
            query = date_table.select() \
                .where(date_table.c.id == id_)
            async for row in connection.execute(query):
                return row

        return await connection_executor(operation)

    @staticmethod
    async def select_time(date1, date2, dancefloor_id):
        async def operation(connection):
            filters = [date_table.c.date.between(date1, date2),
                       date_table.c.dancefloor_id == dancefloor_id, date_table.c.is_busy == True]
            query = date_table.select() \
                .where(and_(*filters))
            query_all = []
            async for row in connection.execute(query):
                query_all.append(row)
            return query_all

        return await connection_executor(operation)

    @staticmethod
    async def select_list(ids_list):
        async def operation(connection):
            filters = []
            for i in ids_list:
                filters.append(date_table.c.id == i)
            query = date_table.select() \
                .where(or_(*filters))
            query_all = []
            async for row in connection.execute(query):
                query_all.append(row)
            return query_all

        return await connection_executor(operation)

    @staticmethod
    async def insert(data):
        async def operation(connection):
            query = date_table.insert() \
                .values(**data)
            await connection.execute(query)

        return await connection_executor(operation)

    @staticmethod
    async def update(id, data):
        async def operation(connection):
            query = date_table.update() \
                .where(date_table.c.id == int(id)) \
                .values(**data)                             # @TODO CHANGE
            await connection.execute(query)

        return await connection_executor(operation)

    @staticmethod
    async def delete(id):
        async def operation(connection):
            query = date_table.delete() \
                .where(date_table.c.id == int(id))          # @TODO CHANGE
            await connection.execute(query)

        return await connection_executor(operation)
