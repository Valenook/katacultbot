import asyncio
import logging
from datetime import time, datetime, timedelta

import aiocache
from aiocache import caches, SimpleMemoryCache
from aiocache.serializers import StringSerializer, PickleSerializer

from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher, filters
from aiogram.utils.executor import start_webhook
from aiogram.utils.callback_data import CallbackData
from aiogram.types.message import ContentType

import aiohttp

from database.dbmodels import Artist, Dancefloor, Schedule, DateTable

from config import config

from helpers.cache import CacheDriver

#cache config
cache_driver = CacheDriver(caches, PickleSerializer)
cache_driver.set_config(config['cache'], 'redis_alt')
#config['cache']['serializer']['class'] = PickleSerializer
#caches.set_config({'redis_alt': config['cache']})
#cache = caches.create('default')

# bot token
API_TOKEN = config['token_service']

# webhook settings
WEBHOOK_HOST = config['webhookurl_service']
WEBHOOK_PATH = ''
WEBHOOK_URL = f"{WEBHOOK_HOST}{WEBHOOK_PATH}"

# webserver settings
WEBAPP_HOST = 'localhost'
WEBAPP_PORT = 5102

logging.basicConfig(level=logging.INFO)

loop = asyncio.get_event_loop()
bot = Bot(token=API_TOKEN, loop=loop)
dp = Dispatcher(bot)


@dp.message_handler(commands=['start'])
async def add_cmd_handler(message: types.Message):
    if await cache_driver.get_cache('status_{}'.format(message.chat.id)):
        button_status = types.KeyboardButton('/status')
        button_push = types.KeyboardButton('/push')
        button_update = types.KeyboardButton('/update')
        button_add = types.KeyboardButton('/add')
        button_remove = types.KeyboardButton('/remove')
        button_ok = types.KeyboardButton('/ok')
        button_cancel = types.KeyboardButton('/cancel')
        button_clear = types.KeyboardButton('/clear')
        #button_logout = types.KeyboardButton('/logout')

        markup = types.ReplyKeyboardMarkup()

        markup.row(button_ok, button_cancel)
        markup.row(button_push, button_update)
        markup.row(button_add, button_remove)
        markup.row(button_status, button_clear)

        await bot.send_message(message.chat.id, 'Привіт, це сервісний бот катакульта, будь-ласка'
                                                ' використовуй наступні команди /status, /add,'
                                                ' /update, /remove, /push, /ok або /cancel, або тисни на кнопки.',
                               reply_markup=markup)


@dp.message_handler(commands=['add'])
async def add_cmd_handler(message: types.Message):
    if await cache_driver.get_cache('status_{}'.format(message.chat.id)):
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton("Add artist", callback_data="add-artist"))
        markup.add(types.InlineKeyboardButton("Add schedule event", callback_data="add-schedule_event"))
        await bot.send_message(message.chat.id, 'Please choose option that you want to add:',
                               reply_markup=markup)


@dp.message_handler(commands=['update'])
async def update_cmd_handler(message: types.Message):
    if await cache_driver.get_cache('status_{}'.format(message.chat.id)):
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton("Update artist (info)", callback_data="update-artist_list"))
        markup.add(types.InlineKeyboardButton("Update schedule event", callback_data="update-schedule_event"))
        await bot.send_message(message.chat.id, 'Please choose option that you want to update:',
                               reply_markup=markup)


@dp.message_handler(commands=['remove'])
async def remove_cmd_handler(message: types.Message):
    if await cache_driver.get_cache('status_{}'.format(message.chat.id)):
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton("Remove artist", callback_data="remove-artist_list"))
        markup.add(types.InlineKeyboardButton("Remove schedule event", callback_data="remove-schedule_event"))
        await bot.send_message(message.chat.id, 'Please choose option that you want remove:',
                               reply_markup=markup)


@dp.message_handler(commands=['status'])
async def status_cmd_handler(message: types.Message):
    if await cache_driver.get_cache('status_{}'.format(message.chat.id)):
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton("Check scene", callback_data="status-scene"))
        markup.add(types.InlineKeyboardButton("Check artist", callback_data="status-artist"))
        await bot.send_message(message.chat.id, 'Hey choose what you want check:', reply_markup=markup)


@dp.message_handler(commands=['p'])
async def pass_check(message: types.Message):
    try:
        _, password = message.text.split(' ')
    except ValueError:
        return

    if password == 'zero2nine':
        button_status = types.KeyboardButton('/status')
        button_push = types.KeyboardButton('/push')
        button_update = types.KeyboardButton('/update')
        button_add = types.KeyboardButton('/add')
        button_remove = types.KeyboardButton('/remove')
        button_ok = types.KeyboardButton('/ok')
        button_cancel = types.KeyboardButton('/cancel')
        button_clear = types.KeyboardButton('/clear')
        #button_logout = types.KeyboardButton('/logout')

        markup = types.ReplyKeyboardMarkup()

        markup.row(button_ok, button_cancel)
        markup.row(button_push, button_update)
        markup.row(button_add, button_remove)
        markup.row(button_status, button_clear)
        await bot.send_message(message.chat.id, 'Привіт, вітаю з успішним логіном в сервісний бот катакульта!'
                                                ' Будь-ласка, використовуй наступні команди:\n'
                                                '/push, щоб зробити розсилку повідомлення\n'
                                                '/add , щоб додати івент або артиста\n'
                                                '/remove, щоб удалити івент або артиста\n'
                                                '/status, щоб подивитися статус сцен або артистів\n'
                                                '/update, щоб оновити інформацію про івенти\n'
                                                'Якщо я перестав відповідати, спробуй натиснути /cancel aбо'
                                                ' /clear, щоб очистити мій кеш та залогінься знову з командою /p. \n'
                                                'Нехай проблеми та незгоди не роблять вам в житті погоди! Гарного дня '
                                                'та хай вам щастить.',
                               reply_markup=markup)
        await cache_driver.set_cache('status_{}'.format(message.chat.id), {'admin': True})


@dp.message_handler(commands=['logout'])
async def logout(message: types.Message):
    print(await cache_driver.get_cache('status_{}'.format(message.chat.id)))
    await cache_driver.clear_cache('status_{}'.format(message.chat.id), 'redis_alt')
    await bot.send_message(message.chat.id, 'Разлогінились, для логіну використай /p "pass"')


@dp.message_handler(commands=['push'])
async def push(message: types.Message):
    if await cache_driver.get_cache('status_{}'.format(message.chat.id)):
        logging.info('Push command pressed')
        await bot.send_message(message.chat.id,
                               'Надішли мені повідомлення, яке потрібно запушити.')
        await cache_driver.set_cache('push', {'push_message': {'message': None,
                                                               'type': None,
                                                               'to_send': False}})


@dp.message_handler(commands=['clear'])
async def clear(message: types.Message):
    await bot.send_message(message.chat.id, 'Очистив кеш та розлогінився. Для логіну використай /p "pass".')
    await cache_driver.set_cache('cache_check',
                                 {'check': {'check_': True}})
    await cache_driver.set_cache('common', {'artists': {}})
    await cache_driver.set_cache('lineup', {'info': {}})


@dp.message_handler(commands=['ok'])
async def status_cmd_handler(message: types.Message):
    if await cache_driver.get_cache('status_{}'.format(message.chat.id)):
        push_dict = await cache_driver.get_cache('push')
        if push_dict.get('push_message'):
            logging.info('Push confirmed: message {}'.format(push_dict.get('push_message').get('message')))
            await cache_driver.update_dict('push', {'push_message': {'message': push_dict.get('push_message').get('message'), 'to_send': True}})
            await bot.send_message(message.chat.id,
                                   'Розпочав розсилати повідомлення "{}"'.format(
                                       push_dict.get('push_message').get('message')))

        elif await cache_driver.get_cache(message.chat.id):
            cache_dict = await cache_driver.get_cache(message.chat.id)
            if cache_dict:
                if cache_dict.get('Artist'):
                    dict_to_save = {}
                    for i, j in cache_dict.get('Artist').items():
                        if j:
                            dict_to_save.update({i: j[:config['database']['string_len'][i]-1]})

                    await Artist\
                        .insert(dict_to_save)

                    await cache_driver.clear_cache(message.chat.id, 'redis_alt')
                    await bot.send_message(message.chat.id, "Artist with\n"
                                                            "Name: {}\n"
                                                            "Short Info: {}\n"
                                                            "Main Url: {}\n"
                                                            "Format: {}\n"
                                                            "Long Info: {}\n"
                                                            "Aux Url: {}\n".
                                           format(cache_dict['Artist']['name'],
                                                  cache_dict['Artist']['info_short'],
                                                  cache_dict['Artist']['url_music_1'],
                                                  cache_dict['Artist']['act_info'],
                                                  cache_dict['Artist']['info_long'],
                                                  cache_dict['Artist']['url_music_2']
                                                  ))
            else:
                await bot.send_message(message.chat.id, 'Нема чого підтверджувати,'
                                                        ' спробуй скористатися командами'
                                                        ' /add, /push "message"')
        else:
            await bot.send_message(message.chat.id, 'Нема чого підтверджувати,'
                                                    ' спробуй скористатися командами'
                                                    ' /add, /push "message"')


@dp.message_handler(commands=['cancel'])
async def status_cmd_handler(message: types.Message):
    if await cache_driver.get_cache('status_{}'.format(message.chat.id)):
        await cache_driver.clear_cache('push', 'redis_alt')
        await cache_driver.clear_cache(message.chat.id, 'redis_alt')
        await bot.send_message(message.chat.id, 'Охорона, відміна!')


@dp.callback_query_handler(filters.Regexp(regexp='(add-([a-z+]+))'))
async def add_callback_handler(query: types.CallbackQuery):
    if await cache_driver.get_cache('status_{}'.format(query.message['chat']['id'])):
        _, callback = query.data.split('-')
        if callback == 'artist':
            await cache_driver.set_cache(query.message['chat']['id'], {'Artist': {"name": None,
                                                                                  "info_short": None,
                                                                                  "info_long": None,
                                                                                  "url_music_1": None,
                                                                                  "url_music_2": None,
                                                                                  "url_1_text": None,
                                                                                  "url_2_text": None,
                                                                                  "act_info": None,
                                                                                  }, "add": None})
            await bot.edit_message_text("Type name of Artist:",
                                  query.message['chat']['id'],
                                  query.message['message_id'],
                                  query.inline_message_id)
        elif 'schedule_event' in callback:
            markup = types.InlineKeyboardMarkup()
            artists = await Artist.select_all(False)
            if artists:
                for artist in artists:
                    markup.add(types.InlineKeyboardButton(artist.name, callback_data="add-artist+{}".format(artist.id)))
                await bot.edit_message_text("Choose artist:".format(),
                                            query.message['chat']['id'], query.message['message_id'],
                                            query.inline_message_id, reply_markup=markup)
            else:
                await bot.edit_message_text("No artist found",
                                            query.message['chat']['id'], query.message['message_id'],
                                            query.inline_message_id)

        elif 'artist' in callback:
            _, name_id = callback.split('+')
            await cache_driver.set_cache(query.message['chat']['id'],
                                         {'add_schedule_event': {'name_id': name_id}})
            markup = types.InlineKeyboardMarkup()
            dancefloors = await Dancefloor.select_all()
            if dancefloors:
                for dancerfloor in dancefloors:
                    markup.add(types.InlineKeyboardButton(dancerfloor.name, callback_data="add-scene+{}".format(dancerfloor.id)))
                await bot.edit_message_text("Choose scene:".format(),
                                            query.message['chat']['id'], query.message['message_id'],
                                            query.inline_message_id, reply_markup=markup)
            else:
                await bot.edit_message_text("No scene found",
                                            query.message['chat']['id'], query.message['message_id'],
                                            query.inline_message_id)

        elif 'scene' in callback:
            _, dancefloor_id = callback.split('+')
            await cache_driver.update_dict(query.message['chat']['id'],
                                           {'add_schedule_event': {'dancefloor_id': dancefloor_id}})
            markup = types.InlineKeyboardMarkup()
            date_info = await DateTable.select(dancefloor_id)
            if date_info:
                for date in date_info:
                    markup.add(types.InlineKeyboardButton(date.date.strftime("%m/%d|%H:%M"), callback_data="add-time+{}".format(date.id)))
                    await bot.edit_message_text("Choose time:".format(),
                                                query.message['chat']['id'], query.message['message_id'],
                                                query.inline_message_id, reply_markup=markup)

        elif 'time' in callback:
            _, date_id = callback.split('+')
            await cache_driver.update_dict(query.message['chat']['id'],
                                           {'add_schedule_event': {'date_id': date_id}})
            cache_dict = await cache_driver.get_cache(query.message['chat']['id'])

            await Schedule.insert({'dancefloor_id': cache_dict['add_schedule_event']['dancefloor_id'],
                                   'artist_id': cache_dict['add_schedule_event']['name_id'],
                                   'date_id': cache_dict['add_schedule_event']['date_id']})
            await DateTable.update(date_id, {'is_busy': True})
            await Artist.update(cache_dict['add_schedule_event']['name_id'], {'date_id': date_id})
            await bot.edit_message_text("Schedule event with artist {} created at scene {} time {} :".
                                        format(cache_dict['add_schedule_event']['name_id'],
                                               cache_dict['add_schedule_event']['dancefloor_id'],
                                               cache_dict['add_schedule_event']['date_id'],),
                                        query.message['chat']['id'], query.message['message_id'],
                                        query.inline_message_id)


@dp.callback_query_handler(filters.Regexp(regexp='(status-([a-z+]+))'))
async def status_callback_handler(query: types.CallbackQuery):
    if await cache_driver.get_cache('status_{}'.format(query.message['chat']['id'])):
        _, callback = query.data.split('-')
        if callback == 'scene':
            markup = types.InlineKeyboardMarkup()
            dancefloors = await Dancefloor.select_all()
            if dancefloors:
                for dancerfloor in dancefloors:
                    markup.add(types.InlineKeyboardButton(dancerfloor.name,
                                                          callback_data="status-scene_events+{}"
                                                          .format(dancerfloor.id)))
                await bot.edit_message_text("Choose scene:".format(),
                                            query.message['chat']['id'], query.message['message_id'],
                                            query.inline_message_id, reply_markup=markup)
            else:
                await bot.edit_message_text("No scene found",
                                            query.message['chat']['id'], query.message['message_id'],
                                            query.inline_message_id)
        if 'scene_events' in callback:
            _, dancerfloor_id = callback.split('+')
            schedule_events = await Schedule.select_all(dancefloor_id=dancerfloor_id)
            dancerfloor = await Dancefloor.select(dancerfloor_id)
            if schedule_events:
                artist_ids = []
                date_ids = []
                for schedule_event in schedule_events:
                    artist_ids.append(schedule_event.artist_id)
                    date_ids.append(schedule_event.date_id)
                artists = await Artist.select_list(artist_ids)
                date_ids = await DateTable.select_list(date_ids)
                message = 'Upcoming events\n'
                for i, j in enumerate(artists):
                    # @TODO Allo tut mojet bit baga s etoy date_ids shlyapoi
                    message += "{}|{}|{}\n".format(dancerfloor.name, artists[i].name, date_ids[i].date)
                await bot.edit_message_text(message,
                                            query.message['chat']['id'], query.message['message_id'],
                                            query.inline_message_id)
            else:
                await bot.edit_message_text("Events not found for dancefloor: {}".format(dancerfloor.name),
                                            query.message['chat']['id'], query.message['message_id'],
                                            query.inline_message_id)
        elif callback == 'artist':
            markup = types.InlineKeyboardMarkup()
            markup.add(types.InlineKeyboardButton("Check artists without events",
                                                  callback_data="status-events+0"))
            markup.add(types.InlineKeyboardButton("Check all artists",
                                                  callback_data="status-events+1"))
            await bot.edit_message_text('Choose what you want check:'.format(),
                                        query.message['chat']['id'], query.message['message_id'],
                                        query.inline_message_id, reply_markup=markup)
        elif 'events+' in callback:
            _, flag = callback.split('+')
            flag = bool(int(flag))
            if flag:
                artists = await Artist.select_all(flag)
                message = 'Next artists in system\n'
                for i in artists:
                    # if i.date_id:                                         @TODO MAKE DATE
                    #     date = await DateTable.select()
                    message += "{}|\t\tDate_id in system: {}\n".format(i.name, i.date_id)
            else:
                artists = await Artist.select_all(flag)
                message = 'Next artists without events in system\n'
                for i in artists:
                    message += "{}\n".format(i.name)
            await bot.edit_message_text(message,
                                        query.message['chat']['id'], query.message['message_id'],
                                        query.inline_message_id)


@dp.callback_query_handler(filters.Regexp(regexp='(remove-([a-z+]+))'))
async def remove_callback_handler(query: types.CallbackQuery):
    if await cache_driver.get_cache('status_{}'.format(query.message['chat']['id'])):
        _, callback = query.data.split('-')
        if callback == 'artist_list':
            markup = types.InlineKeyboardMarkup()
            artists = await Artist.select_all()
            if artists:
                for artist in artists:
                    markup.add(types.InlineKeyboardButton(artist.name, callback_data="remove-artist+{}".format(artist.id)))
                await bot.edit_message_text("Choose artist:".format(),
                                            query.message['chat']['id'], query.message['message_id'],
                                            query.inline_message_id, reply_markup=markup)
            else:
                await bot.edit_message_text("No artist found",
                                            query.message['chat']['id'], query.message['message_id'],
                                            query.inline_message_id)
        elif 'artist+' in callback:
            _, artist_id = callback.split('+')
            artist = await Artist.select(id=artist_id)
            if artist:
                if artist.date_id:
                    await DateTable.update(artist.date_id, {'is_busy': False})
                    await Schedule.delete(date_id=artist.date_id)
                    await Artist.delete(id=artist_id)
                    await bot.edit_message_text("Artist {} removed; Events with artist removed also"
                                                .format(artist.name),
                                                query.message['chat']['id'],
                                                query.message['message_id'],
                                                query.inline_message_id)
                else:
                    await Artist.delete(id=artist_id)
                    await bot.edit_message_text("Artist {} removed; Events without changes"
                                                .format(artist.name),
                                                query.message['chat']['id'],
                                                query.message['message_id'],
                                                query.inline_message_id)
            else:
                await bot.edit_message_text("Artist already removed",
                                            query.message['chat']['id'], query.message['message_id'],
                                            query.inline_message_id)
        elif 'schedule_event' == callback:
            markup = types.InlineKeyboardMarkup()
            dancefloors = await Dancefloor.select_all()
            if dancefloors:
                for dancerfloor in dancefloors:
                    markup.add(types.InlineKeyboardButton(dancerfloor.name,
                                                          callback_data="remove-scene+{}"
                                                          .format(dancerfloor.id)))
                await bot.edit_message_text("Choose scene:".format(),
                                            query.message['chat']['id'], query.message['message_id'],
                                            query.inline_message_id, reply_markup=markup)
            else:
                await bot.edit_message_text("No scene found",
                                            query.message['chat']['id'], query.message['message_id'],
                                            query.inline_message_id)
        elif 'scene+' in callback:
            _, dancerfloor_id = callback.split('+')
            markup = types.InlineKeyboardMarkup()
            schedule_events = await Schedule.select_all(dancefloor_id=dancerfloor_id)
            dancerfloor = await Dancefloor.select(dancerfloor_id)
            if schedule_events:
                artist_ids = []
                date_ids = []
                for schedule_event in schedule_events:
                    artist_ids.append(schedule_event.artist_id)
                    date_ids.append(schedule_event.date_id)
                artists = await Artist.select_list(artist_ids)
                date_ids = await DateTable.select_list(date_ids)
                for i, j in enumerate(artists):
                    markup.add(types.InlineKeyboardButton(
                        "{}|{}|{}".format(dancerfloor.name, artists[i].name,
                                          [item for item in date_ids if item[0] == artists[i].date_id][0][1]),
                        callback_data="remove-schedule_event+{}"
                            .format(artists[i].id)))
                await bot.edit_message_text("Choose event:".format(),
                                            query.message['chat']['id'], query.message['message_id'],
                                            query.inline_message_id, reply_markup=markup)

        elif 'schedule_event+' in callback:
            _, artist_id = callback.split('+')
            artist = await Artist.select(id=artist_id)
            await Artist.update(artist_id, {'date_id': None})
            await DateTable.update(artist.date_id, {'is_busy': False})
            await Schedule.delete(date_id=artist.date_id)
            await bot.edit_message_text("Event removed".format(),
                                        query.message['chat']['id'], query.message['message_id'],
                                        query.inline_message_id)


@dp.callback_query_handler(filters.Regexp(regexp='(update-([a-z+]+))'))
async def update_callback_handler(query: types.CallbackQuery):
    if await cache_driver.get_cache('status_{}'.format(query.message['chat']['id'])):
        _, callback = query.data.split('-')
        if callback == 'artist_list':
            await bot.send_message(query.message['chat']['id'],
                                   "На жаль, змінити інфо про артистів наразі неможливо.")
            """
            markup = types.InlineKeyboardMarkup()

            artists = await Artist.select_all()
            if artists:
                for artist in artists:
                    print('artist.name', artist.name)
                    markup.add(types.InlineKeyboardButton(artist.name, callback_data="update-artist+{}".format(artist.id)))
                await bot.send_message(query.message['chat']['id'],
                                       "Choose the artist you want to update:".format(),
                                       reply_markup=markup)
            else:
                await bot.edit_message_text("Sorry, no artist found",
                                            query.message['chat']['id'], query.message['message_id'],
                                            query.inline_message_id)
            """
        elif callback == 'schedule_event':
            markup = types.InlineKeyboardMarkup()
            dancefloors = await Dancefloor.select_all()
            if dancefloors:
                for dancerfloor in dancefloors:
                    markup.add(types.InlineKeyboardButton(dancerfloor.name,
                                                          callback_data="update-scene_events+{}"
                                                          .format(dancerfloor.id)))
                await bot.send_message(query.message['chat']['id'],
                                       "Choose scene:".format(),
                                       reply_markup=markup)
            else:
                await bot.send_message(query.message['chat']['id'],
                                       "No scene found")

        elif 'scene_events+' in callback:
            _, dancerfloor_id = callback.split('+')
            markup = types.InlineKeyboardMarkup()
            schedule_events = await Schedule.select_all(dancefloor_id=dancerfloor_id)
            dancerfloor = await Dancefloor.select(dancerfloor_id)
            if schedule_events:
                artist_ids = []
                date_ids = []
                for schedule_event in schedule_events:
                    artist_ids.append(schedule_event.artist_id)
                    date_ids.append(schedule_event.date_id)
                artists = await Artist.select_list(artist_ids)
                date_ids = await DateTable.select_list(date_ids)
                message = 'Обирай івент, який треба змінити:\n'
                for i, j in enumerate(artists):
                    markup.add(types.InlineKeyboardButton(
                        "{}|{}|{}".format(dancerfloor.name, artists[i].name, [item for item in date_ids if item[0] == artists[i].date_id][0][1]),
                        callback_data="update-timechange+{}"
                        .format(artists[i].date_id)))
                await bot.send_message(query.message['chat']['id'],
                                       message,
                                       reply_markup=markup)
            else:
                await bot.send_message(query.message['chat']['id'],
                                       "Фіча недоступна")

        elif 'timechange+' in callback:

            _, date_id = callback.split('+')
            date_record = await DateTable.select_id(int(date_id))
            message = "Змінюємо час виступу {}".format(date_record.date)
            markup = types.InlineKeyboardMarkup()
            markup.add(types.InlineKeyboardButton('-30 хвилин', callback_data="update-timedelta+{}|m30".format(date_id)))
            markup.add(types.InlineKeyboardButton('-15 хвилин', callback_data="update-timedelta+{}|m15".format(date_id)))
            markup.add(types.InlineKeyboardButton('+15 хвилин', callback_data="update-timedelta+{}|p15".format(date_id)))
            markup.add(types.InlineKeyboardButton('+30 хвилин', callback_data="update-timedelta+{}|p30".format(date_id)))
            await bot.edit_message_text(message,
                                        query.message['chat']['id'],
                                        query.message['message_id'],
                                        query.inline_message_id, reply_markup=markup)

        elif 'timedelta+' in callback:
            aux_dict = {'m30': -30, 'm15': -15, 'p15': 15, 'p30': 30}
            _, aux_value = callback.split('+')
            date_id, diff = aux_value.split('|')
            time_ = aux_dict.get(diff)
            print()
            date_record = await DateTable.select_id(int(date_id))
            new_time_ = date_record.date + timedelta(minutes=time_)
            print('ALLLLLLLLO****************************')
            print(date_record.date, new_time_)
            await DateTable.update(int(date_id), {'date': new_time_})
            print((await DateTable.select_id(int(date_id))).date)
            print('ALLLLLLLLO****************************')

            message = "Змінили час виступу на наступний {}".format(new_time_)
            await bot.edit_message_text(message,
                                        query.message['chat']['id'],
                                        query.message['message_id'],
                                        query.inline_message_id)
            """
            await bot.edit_message_text("Фіча недоступна",
                                        query.message['chat']['id'], query.message['message_id'],
                                        query.inline_message_id)
                                        """


@dp.message_handler(content_types=ContentType.ANIMATION)
async def gif_handler(message: types.Message):
    if await cache_driver.get_cache('status_{}'.format(message.chat.id)):
        push_dict = await cache_driver.get_cache('push')
        if push_dict.get('push_message').get('message') is None:
            await bot.send_message(message.chat.id,
                                   'Я готовий пушнути повідомлення "{}". Натисни /ok або створи нове командою /push'.format(
                                       'InputMediaAnimation ' + message.animation.file_id))

            api = 'https://api.telegram.org/bot{}/getFile?file_id={}'.format(API_TOKEN, message.animation.file_id)
            async with aiohttp.request("GET", api) as r:
                result = await r.json()
                #(result)

            await cache_driver.update_dict('push', {
                'push_message': {'message': 'https://telegram.org/' + result.get('result').get('file_path'),
                                 'type': message.content_type, 'to_send': False}})


@dp.message_handler()
async def message_hadler(message: types.Message):
    if await cache_driver.get_cache('status_{}'.format(message.chat.id)):
        push_dict = await cache_driver.get_cache('push')
        cache_dict = await cache_driver.get_cache(message.chat.id)
        if 'add' in cache_dict.keys():
            if not cache_dict['Artist']['name']:
                await cache_driver.update_dict(message.chat.id,
                                               {'Artist': {'name': message.text}})
                await bot.send_message(message.chat.id, "Type short info about Artist: (<250 symbols)\n"
                                                        "or use /ok to add only this info")
            elif not cache_dict['Artist']['info_short']:
                await cache_driver.update_dict(message.chat.id,
                                               {'Artist': {'info_short': message.text}})
                await bot.send_message(message.chat.id, "Type Artist's main url: (<128 symbols)\n"
                                                        "or use /ok to add only this info")
            elif not cache_dict['Artist']['url_music_1']:
                await cache_driver.update_dict(message.chat.id,
                                               {'Artist': {'url_music_1': message.text}})
                await bot.send_message(message.chat.id, "Type Artist's text for url-mask-1: (<250 symbols)\n"
                                                        "or use /ok to add only this info")

            elif not cache_dict['Artist']['url_1_text']:
                await cache_driver.update_dict(message.chat.id,
                                               {'Artist': {'url_1_text': message.text}})
                await bot.send_message(message.chat.id, "Type Artist's format: (<250 symbols)\n"
                                                        "or use /ok to add only this info")

            elif not cache_dict['Artist']['act_info']:
                await cache_driver.update_dict(message.chat.id,
                                               {'Artist': {'act_info': message.text}})
                await bot.send_message(message.chat.id, "Type info about Artist: (<800 symbols)\n"
                                                        "or use /ok to add only this info")
            elif not cache_dict['Artist']['info_long']:
                await cache_driver.update_dict(message.chat.id,
                                               {'Artist': {'info_long': message.text}})
                await bot.send_message(message.chat.id, "Type Artist's aux url: (<128 symbols)\n"
                                                        "or use /ok to add only this info")
            elif not cache_dict['Artist']['url_music_2']:
                await cache_driver.update_dict(message.chat.id,
                                               {'Artist': {'url_music_2': message.text}})
                await bot.send_message(message.chat.id,
                                       "Type Artist's text for url-mask-2: (<250 symbols)\n"
                                       "or use /ok to add only this info")

            elif not cache_dict['Artist']['url_2_text']:
                await cache_driver.update_dict(message.chat.id,
                                               {'Artist': {'url_2_text': message.text}})

                final_cache = await cache_driver.get_cache(message.chat.id)
                await bot.send_message(message.chat.id, "Okay, you want add next artist\n"
                                                        "Name: {}\n"
                                                        "Short Info: {}\n"
                                                        "Main Url: {}\n"
                                                        "Format: {}\n"
                                                        "Long Info: {}\n"
                                                        "Aux Url: {}\n"
                                                        "If it's okay? Use /ok or /cancel".
                                       format(final_cache['Artist']['name']
                                              [:config['database']['string_len']['name']-1],
                                              final_cache['Artist']['info_short']
                                              [:config['database']['string_len']['info_short']-1],
                                              final_cache['Artist']['url_music_1']
                                              [:config['database']['string_len']['url_music_1']-1],
                                              final_cache['Artist']['act_info']
                                              [:config['database']['string_len']['act_info']-1],
                                              final_cache['Artist']['info_long']
                                              [:config['database']['string_len']['info_long'] - 1],
                                              final_cache['Artist']['url_music_2']
                                              [:config['database']['string_len']['url_music_2'] - 1]
                                              ))

            else:
                await bot.send_message(message.chat.id, "Please use /ok or /cancel for add or reject add"
                                                        " artist with\nName: {}\nInfo: {}\nUrl: {}\n"
                                                        "Format: {}\nWas added".format(
                                                            cache_dict['Artist']['name'],
                                                            cache_dict['Artist']['info_short'],
                                                            cache_dict['Artist']['url_music_1'],
                                                            cache_dict['Artist']['act_info']))

        elif push_dict.get('push_message').get('message') is None:
            await bot.send_message(message.chat.id,
                                   'Я готовий пушнути повідомлення "{}". Натисни /ok або створи нове командою /push'.format(
                                       message.text))
            await cache_driver.update_dict('push', {
                'push_message': {'message': message.text, 'type': 'text', 'to_send': False}})
        else:
            await bot.send_message(message.chat.id, "Я не очікував від тебе повідомлень. Тисни на кнопки⬇")
    elif await cache_driver.get_cache(message.chat.id):
        cache_dict = await cache_driver.get_cache(message.chat.id)
        #print('push_dict', push_dict)
        print('cache_dict', cache_dict)



async def on_startup(dp):
    # cache_driver.cache_create('default')
    cache_driver.cache_create('redis_alt')
    await bot.set_webhook(WEBHOOK_URL)



async def on_shutdown(dp):
    pass


if __name__ == '__main__':
    start_webhook(dispatcher=dp, webhook_path=WEBHOOK_PATH, on_startup=on_startup, on_shutdown=on_shutdown,
                  skip_updates=True, host=WEBAPP_HOST, port=WEBAPP_PORT)
