from datetime import datetime, date, time

import asyncio
import logging

import aiocache
from aiocache import caches, SimpleMemoryCache
from aiocache.serializers import StringSerializer, PickleSerializer

from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher, filters
from aiogram.dispatcher.webhook import get_new_configured_app
from aiogram.utils.executor import start_webhook
from aiogram.utils import executor
from aiogram.types.message import ContentType

from database.dbmodels import Artist, Dancefloor, Schedule, User, Notification, DateTable
from config import config

from helpers.cache import CacheDriver
from helpers.help_functions import str2datetime, time_scenes, day2week, scene_info
from helpers.chatbase import ChatbaseSender, Message

# cache config
cache_driver = CacheDriver(caches, PickleSerializer)
cache_driver.set_config(config['cache'])
# cache_driver.set_config(config['cache']['default'], 'default')
# cache_driver.set_config(config['cache']['redis_alt'], 'redis_alt')

# config['cache']['serializer']['class'] = PickleSerializer
# caches.set_config({'default': config['cache']})
# cache = caches.create('default')

# Chatbase
chatbase_sender = ChatbaseSender(auth_key=config['chatbase_key'])

# bot token
API_TOKEN = config['token_user']

# webhook settings
WEBHOOK_HOST = config['webhookurl_user']
WEBHOOK_PATH = ''
WEBHOOK_URL = f"{WEBHOOK_HOST}{WEBHOOK_PATH}"

# webserver settings
WEBAPP_HOST = 'localhost'
WEBAPP_PORT = 5101

# payments token
PAYMENTS_PROVIDER_TOKEN = config['PAYMENTS_PROVIDER_TOKEN']

logging.basicConfig(level=logging.INFO)

loop = asyncio.get_event_loop()
bot = Bot(token=API_TOKEN, loop=loop)
dp = Dispatcher(bot, loop=loop)

key_id_map = {'allo': 'allo_payload'}

async def check_in_database(chat_id, user_id):
    #print(user_id)
    cache_dict = await cache_driver.get_cache(chat_id)
    #print(cache_dict)
    if not cache_dict.get('user_status'):
        print('not cached')
        user = await User.select(tg_chat_id=str(chat_id))
        if not user:
            # await User.insert({'tg_chat_id': str(chat_id), 'tg_id': str(user_id)})
            await User.insert({'tg_chat_id': str(chat_id), 'is_push_forbidden': False})
            await cache_driver.set_cache(chat_id, {'user_status': {'is_cached': True}})
        else:
            await cache_driver.set_cache(chat_id, {'user_status': {'is_cached': True}})

@dp.message_handler(regexp='(нес(е|ё|оь)(т|ть)ся)+|(муз(ы|и)ка)+|(лайн(-ап|ап))+|(сц(і|е)на)+'
                           '|([Dd]vor|([Дд]в(о|і)р))|(3(rd|-rd))|([Cc]loser)|([Mm]e(zz|z)anine'
                           '|([Мм]е(з|зз)ан(і|и)н))|([Oo]tel|(([Оо])тель))|(([Ll]esnoy)|([Лл]('
                           'е|і)сн(и|о)й)|([Pp]richal)|([Пп]р(и|і)чал))|([Mm]etaculture|['
                           'Мм]етакультура)')
async def start_message_handler(message: types.Message):
    await check_in_database(message.chat.id, message.from_user.id)
    user_message = Message('telegram', message.text, 'smalltalk', '0.01',
                           message.from_user.id, 'user')
    await chatbase_sender.send_message(user_message)
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Артисти", callback_data="info"))
    markup.add(types.InlineKeyboardButton("Лайнап", callback_data="lineup"))
    markup.add(types.InlineKeyboardButton("Шо несеться?", callback_data="now"))
    await bot.send_message(message.chat.id, 'Так, доповідаю:', reply_markup=markup)


@dp.message_handler(commands=['start'])
async def start_handler(message: types.Message):
    await check_in_database(message.chat.id, message.from_user.id)
    user_message = Message('telegram', message.text, 'start', '0.01',
                           message.from_user.id, 'user')
    await chatbase_sender.send_message(user_message)
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Привіт", callback_data="hello"))
    await bot.send_message(message.chat.id, 'Привіт! Я @katafest_bot!\nВзагалі-то, я збирався горіти в ці вихідні під правою колонкою, але вирішив, що мені буде прикольніше висіти з тобою тут.',
                           reply_markup=markup)

@dp.callback_query_handler(filters.Regexp(regexp='(hello)'))
async def hello_handler(query: types.CallbackQuery):
    """
    Хендлер callbacka
    """
    await check_in_database(query.message['chat']['id'], query.from_user.id)
    user_message = Message('telegram', query.data, 'hello', '0.01',
                           query.message['chat']['id'], 'user')
    await chatbase_sender.send_message(user_message)
    markup = types.InlineKeyboardMarkup()
    await bot.edit_message_text('Я всіх знаю, підкажу хто де грає зараз і коли вийде' 
                                ' потрібний музикант.\nЯкщо станеться якийсь ' 
                                'форс-мажор - я маякну!\n ',
                                query.message['chat']['id'], query.message['message_id'],
                                query.inline_message_id)
    markup.add(types.InlineKeyboardButton("Артисти", callback_data="info"))
    markup.add(types.InlineKeyboardButton("Лайнап", callback_data="lineup"))
    markup.add(types.InlineKeyboardButton("Шо несеться?", callback_data="now"))
    await bot.send_message(query.message['chat']['id'], 'Давай оберемо щось зі списку:',
                           reply_markup=markup)


@dp.message_handler(content_types=ContentType.STICKER)
async def gif_handler(message: types.Message):
    if message.sticker.set_name == 'gariki':
        await bot.send_message(message.chat.id,
                               'Отримав стікера "{}" з правильного пака. Привєт пацани'.format(message.sticker.file_id))
    else:
        #await bot.send_message(message.chat.id,
        #                       'Отримав стікера з неправильного пака. Ветра не буде '.format(
        #                           message.sticker.file_id))
        await bot.send_sticker(message.chat.id, sticker="CAADAgAD0QADNuwbBQltUpd9jwABLAI")


@dp.message_handler(regexp='(есть ч(е|ё|о))|(ч(о|е|ё|ьо))|(ч(о|е|ё|ьо)т)+')
async def smalltalk1_handler(message: types.Message):
    await check_in_database(message.chat.id, message.from_user.id)
    user_message = Message('telegram', message.text, 'smalltalk', '0.01',
                           message.from_user.id, 'user')
    await chatbase_sender.send_message(user_message)
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Меню", callback_data="menu"))
    await bot.send_message(message.chat.id, 'Не чьо, а що, читай мовний закон, дядя. Ти ж в Києві.',
                           reply_markup=markup)


@dp.message_handler(regexp='(д(а|о)ро(г|ж|жк)а)+|(троп(а|инка|ка))+')
async def smalltalk2_handler(message: types.Message):
    await check_in_database(message.chat.id, message.from_user.id)
    user_message = Message('telegram', message.text, 'smalltalk', '0.01',
                           message.from_user.id, 'user')
    await chatbase_sender.send_message(user_message)
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Меню", callback_data="menu"))
    await bot.send_message(message.chat.id, 'Дорогу здолає той, хто йде.', reply_markup=markup)


@dp.message_handler(regexp='((шо|що) (ти|ты))+|(дядя)+')
async def smalltalk3_handler(message: types.Message):
    await check_in_database(message.chat.id, message.from_user.id)
    user_message = Message('telegram', message.text, 'smalltalk', '0.01',
                           message.from_user.id, 'user')
    await chatbase_sender.send_message(user_message)
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Меню", callback_data="menu"))
    await bot.send_message(message.chat.id, 'Всі горять на танцполі, а ти у телефоні :( ',
                           reply_markup=markup)


@dp.message_handler(regexp='(к(о|а)л(е|оь|ё)с(о|а))+|(табл)+')
async def smalltalk4_handler(message: types.Message):
    await check_in_database(message.chat.id, message.from_user.id)
    user_message = Message('telegram', message.text, 'smalltalk', '0.01',
                           message.from_user.id, 'user')
    await chatbase_sender.send_message(user_message)
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Меню", callback_data="menu"))
    await bot.send_message(message.chat.id, 'А куди ти на ньому поїдеш?', reply_markup=markup)

"""
@dp.message_handler(regexp='(слу(шать|хат))+|(Лещенко)|(Паш)')
async def smalltalk5_handler(message: types.Message):
    await check_in_database(message.chat.id, message.from_user.id)
    user_message = Message('telegram', message.text, 'smalltalk', '0.01',
                           message.from_user.id, 'user')
    await chatbase_sender.send_message(user_message)
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Аліна Паш", callback_data="none"))      # @TODO !
    markup.add(types.InlineKeyboardButton("Сергій Лещенко", callback_data="none")) # @TODO !
    markup.add(types.InlineKeyboardButton("Меню", callback_data="menu"))
    await bot.send_message(message.chat.id, 'Дядя, я ж просто бот. А от мої кенти точно знають,'
                                            ' кого послухати.', reply_markup=markup)
"""


@dp.message_handler(regexp='(п(о|а)пуст(и|і)т)+|((о|а)тпуст(и|і)т)+|(попус)+|(відпуст(и|і)т)+')
async def smalltalk4_handler(message: types.Message):
    await check_in_database(message.chat.id, message.from_user.id)
    user_message = Message('telegraпm', message.text, 'smalltalk', '0.01',
                           message.from_user.id, 'user')
    await chatbase_sender.send_message(user_message)
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Меню", callback_data="menu"))
    await bot.send_message(message.chat.id, 'Дядя, сам кабіну збираю )', reply_markup=markup)


@dp.message_handler(regexp='(лох)+|(п(и|і)дар)+|(п(і|и)др)+|(чм(о|ы))+|(за(е|ї)ба(в|л))+|(п(і|и|е)зд)+|(д(о|а)(в|л)б)+|'
                           '(ху(й|е))+|(ч(и|е|і)пуш)+|(курва)+|(йолоп)+|(сос(і|и))+|(мурло)')
async def smalltalk4_handler(message: types.Message):
    await check_in_database(message.chat.id, message.from_user.id)
    user_message = Message('telegraпm', message.text, 'smalltalk', '0.01',
                           message.from_user.id, 'user')
    await chatbase_sender.send_message(user_message)
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Меню", callback_data="menu"))
    await bot.send_message(message.chat.id, 'Я думав, ти кєнт, — а ти гірший, ніж мєнт.', reply_markup=markup)


@dp.message_handler(regexp='(н(і|и)хуя)+|(бля)+|(сука)+|((е|ї)ба(в|л))+|(наху(я|й))|(ска)')
async def smalltalk4_handler(message: types.Message):
    await check_in_database(message.chat.id, message.from_user.id)
    user_message = Message('telegraпm', message.text, 'smalltalk', '0.01',
                           message.from_user.id, 'user')
    await chatbase_sender.send_message(user_message)
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Меню", callback_data="menu"))
    await bot.send_message(message.chat.id, 'Не матюкайся, заради Христа!', reply_markup=markup)


@dp.message_handler(regexp='(слава Укра(ї|і|и)н(і|е|и))+')
async def smalltalk4_handler(message: types.Message):
    await check_in_database(message.chat.id, message.from_user.id)
    user_message = Message('telegraпm', message.text, 'smalltalk', '0.01',
                           message.from_user.id, 'user')
    await chatbase_sender.send_message(user_message)
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Меню", callback_data="menu"))
    await bot.send_message(message.chat.id, 'Героям слава!', reply_markup=markup)


@dp.message_handler(regexp='(слава нац(|і|и)(ї|і|е))+')
async def smalltalk4_handler(message: types.Message):
    await check_in_database(message.chat.id, message.from_user.id)
    user_message = Message('telegraпm', message.text, 'smalltalk', '0.01',
                           message.from_user.id, 'user')
    await chatbase_sender.send_message(user_message)
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Меню", callback_data="menu"))
    await bot.send_message(message.chat.id, 'Смерть ворогам!', reply_markup=markup)


@dp.message_handler(regexp='(Україна)')
async def smalltalk4_handler(message: types.Message):
    await check_in_database(message.chat.id, message.from_user.id)
    user_message = Message('telegraпm', message.text, 'smalltalk', '0.01',
                           message.from_user.id, 'user')
    await chatbase_sender.send_message(user_message)
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Меню", callback_data="menu"))
    await bot.send_message(message.chat.id, 'Понад усе!', reply_markup=markup)


@dp.message_handler(regexp='(маже)+')
async def smalltalk4_handler(message: types.Message):
    await check_in_database(message.chat.id, message.from_user.id)
    user_message = Message('telegraпm', message.text, 'smalltalk', '0.01',
                           message.from_user.id, 'user')
    await chatbase_sender.send_message(user_message)
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Меню", callback_data="menu"))
    await bot.send_message(message.chat.id, 'Я ж казав — половинку.', reply_markup=markup)


@dp.message_handler(regexp='(хочеш)+')
async def smalltalk4_handler(message: types.Message):
    await check_in_database(message.chat.id, message.from_user.id)
    user_message = Message('telegraпm', message.text, 'smalltalk', '0.01',
                           message.from_user.id, 'user')
    await chatbase_sender.send_message(user_message)
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Меню", callback_data="menu"))
    await bot.send_message(message.chat.id, 'Ти шо — я ж на роботі.', reply_markup=markup)


"""
@dp.message_handler(regexp='(купить)')
async def process_payment(message: types.Message):
    if PAYMENTS_PROVIDER_TOKEN.split(':')[1] == 'TEST':
        await bot.send_message(message.chat.id, 'Купи тестовый билет')

    price = types.LabeledPrice(label='buy me', amount=10050000)

    await bot.send_invoice(message.chat.id,
                           title='БИЛЕТ НА БРЕЙВ',
                           description='ОПИСАНИЕ БИЛЕТА НА БРЕЙВ',
                           provider_token=PAYMENTS_PROVIDER_TOKEN,
                           currency='uah',
                           photo_url='https://scontent-mxp1-1.xx.fbcdn.net/v/t1.0-9/40082768_2202582689772766_1925814291294846976_n.jpg?_nc_cat=109&_nc_ht=scontent-mxp1-1.xx&oh=625fc7d6806b12ebe5ad3b4aee7b433c&oe=5D7D46BF',
                           photo_height=512,  # !=0/None, иначе изображение не покажется
                           photo_width=512,
                           photo_size=512,
                           is_flexible=False,  # True если конечная цена зависит от способа доставки
                           need_email=True,
                           need_phone_number=True,
                           need_shipping_address=False,
                           prices=[price],
                           start_parameter='time-machine-example',
                           payload='some-invoice-payload-for-our-internal-use'
                           )


@dp.message_handler(filters.ContentTypeFilter(ContentType.SUCCESSFUL_PAYMENT))
async def process_successful_payment(message: types.Message):
    print('successful_payment:')
    pmnt = message.successful_payment.to_python()
    for key, val in pmnt.items():
        print(f'{key} = {val}')

    await bot.send_message(
        message.chat.id,
        'Платеж на сумму `{total_amount} {currency}` совершен успешно! '.format(
            total_amount=message.successful_payment.total_amount // 100,
            currency=message.successful_payment.currency
        )
    )


@dp.pre_checkout_query_handler(func=lambda query: True)
async def process_pre_checkout_query(pre_checkout_query: types.PreCheckoutQuery):
    await bot.answer_pre_checkout_query(pre_checkout_query.id, ok=True)
    #TODO save all info about payment here
"""


@dp.callback_query_handler(filters.Regexp(regexp='(menu)'))
async def menu_callback_handler(query: types.CallbackQuery):
    """"""
    await check_in_database(query.message['chat']['id'], query.from_user.id)
    user_message = Message('telegram', query.data, 'menu', '0.01',
                           query.message['chat']['id'], 'user')
    await chatbase_sender.send_message(user_message)
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Артисти", callback_data="info"))
    markup.add(types.InlineKeyboardButton("Лайнап", callback_data="lineup"))
    markup.add(types.InlineKeyboardButton("Шо несеться?", callback_data="now"))
    await bot.send_message(query.message['chat']['id'],
                           "Давай оберемо щось зі списку",
                           reply_markup=markup)


@dp.callback_query_handler(filters.Regexp(regexp='(info)'))
async def info_callback_handler(query: types.CallbackQuery):
    """"""
    await check_in_database(query.message['chat']['id'], query.from_user.id)
    user_message = Message('telegram', query.data, 'info', '0.01',
                           query.message['chat']['id'], 'user')
    await chatbase_sender.send_message(user_message)
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Знаю артиста", callback_data="artist"))
    markup.add(types.InlineKeyboardButton("Знаю сцену", callback_data="stage"))
    markup.add(types.InlineKeyboardButton("Шо несеться?", callback_data="now"))
    await bot.send_message(query.message['chat']['id'], "Цього року на Black Factory 21 виступ і 3 сцени. Обирай:",
                           reply_markup=markup)


@dp.callback_query_handler(filters.Regexp(regexp='(lineup)'))
async def lineup_callback_handler(query: types.CallbackQuery):
    """"""
    await check_in_database(query.message['chat']['id'], query.from_user.id)
    user_message = Message('telegram', query.data, 'lineup', '0.01',
                           query.message['chat']['id'], 'user')
    await chatbase_sender.send_message(user_message)
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Покажи сцену", callback_data="stage"))
    markup.add(types.InlineKeyboardButton("Шо несеться?", callback_data="now"))
    markup.add(types.InlineKeyboardButton("Меню", callback_data="menu"))

    await bot.send_message(query.message['chat']['id'],
                           "Обери сцену. Або глянь, що несеться зараз на кожній.",
                           reply_markup=markup)


@dp.callback_query_handler(filters.Regexp(regexp='(now)'))
async def now_callback_handler(query: types.CallbackQuery):
    """"""
    await check_in_database(query.message['chat']['id'], query.from_user.id)
    user_message = Message('telegram', query.data, 'now', '0.01',
                           query.message['chat']['id'], 'user')
    await chatbase_sender.send_message(user_message)
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Назад", callback_data="lineup"))
    markup.add(types.InlineKeyboardButton("Меню", callback_data="menu"))

    datetime_1 = datetime.combine(date(2019, 5, 18), time(21, 0))
    datetime_2 = datetime.now()

    timecheck_cache = await cache_driver.get_cache('lineup')

    if not timecheck_cache.get('info'):
        cache_scenes = await cache_driver.get_cache('scenes')

        message = "Ой, шо несеться:\n"
        for i in ['Forma', 'Tseh', 'Topka']:
            #for i in ['Angar', 'Cement', 'Depo', 'Topka', 'Garden', 'Tokar']:
            #for i in ['Dvor', '3-rd', 'Closer', 'Mezzanine', 'Otel', 'Lesnoy Prichal', 'Metaculture']:
            date_times = await DateTable.select_time(datetime_1, datetime_2, int(cache_scenes[i]))
            date_times.sort(key=lambda r: r.date)
            if date_times:
                datetime_r = date_times[-1]
                artist = await Artist.select(date_id=datetime_r.id)
                if artist and time_scenes.get(i).get('time') > datetime_2:
                    message += "<strong>{}</strong> - <i>{}</i>, з {}\n".format(i, artist.name, datetime_r.date.strftime("%H:%M"))
                else:
                    message += "<strong>{}</strong> - тиша \n".format(i)
            else:
                message += "<strong>{}</strong> - тиша\n".format(i)

        cache_dict = {'info': {'message': message, 'day': datetime_2.day, 'hour': datetime_2.day,
                               'is_half': True if datetime_2.minute > 30 else False}}
        await cache_driver.update_dict('lineup', cache_dict)
    else:
        #print('d', timecheck_cache.get('info').get('day'), datetime_2.day)
        #print('h', timecheck_cache.get('info').get('hour'), datetime_2.hour)
        aux_dict = timecheck_cache.get('info')
        if ((datetime_2.day != aux_dict.get('day')) or\
                (datetime_2.hour != aux_dict.get('hour')) or\
                ((True if datetime_2.minute > 30 else False) is not aux_dict.get('is_half'))):
            cache_scenes = await cache_driver.get_cache('scenes')
            #print('not_cached line')
            message = "Ой, шо несеться:\n"
            for i in ['Forma', 'Tseh', 'Topka']:
                #for i in ['Angar', 'Cement', 'Depo', 'Topka', 'Garden', 'Tokar']:
                #for i in ['Dvor', '3-rd', 'Closer', 'Mezzanine', 'Otel', 'Lesnoy Prichal',
                #          'Metaculture']:
                date_times = await DateTable.select_time(datetime_1, datetime_2,
                                                         int(cache_scenes[i]))
                date_times.sort(key=lambda r: r.date)
                if date_times:
                    datetime_r = date_times[-1]
                    #print(datetime_r)
                    artist = await Artist.select(date_id=datetime_r.id)
                    #print(artist)
                    if artist and time_scenes.get(i).get('time') > datetime_2:
                        message += "<strong>{}</strong> - <i>{}</i>, з {}\n".\
                            format(i, artist.name, datetime_r.date.strftime("%H:%M"))
                    else:
                        message += "<strong>{}</strong> - тиша \n".format(i)
                else:
                    message += "<strong>{}</strong> - тиша\n".format(i)

            cache_dict = {
                'info': {'message': message, 'day': datetime_2.day, 'hour': datetime_2.day,
                         'is_half': True if datetime_2.minute > 30 else False}}
            await cache_driver.update_dict('lineup', cache_dict)

        else:
            message = aux_dict.get('message')
            #print('cached line')
    await bot.send_message(query.message['chat']['id'],
                           message,
                           reply_markup=markup,
                           parse_mode='html')


@dp.callback_query_handler(filters.Regexp(regexp='(stage_check:)'))
async def stage_dedicated_callback_handler(query: types.CallbackQuery):
    """"""
    await check_in_database(query.message['chat']['id'], query.from_user.id)
    user_message = Message('telegram', query.data, 'stage_check', '0.01',
                           query.message['chat']['id'], 'user')
    await chatbase_sender.send_message(user_message)
    _, callback = query.data.split(':')
    id_, scene = callback.split('|')
    cache_dict = await cache_driver.get_cache('common')
    schedule_events = await Schedule.select_all(dancefloor_id=id_)
    artist_ids = []
    search_ids = []
    for schedule_event in schedule_events:
        artist_ids.append(schedule_event.artist_id)
        if not cache_dict.get('artists').get(str(schedule_event.artist_id)):
            search_ids.append(schedule_event.artist_id)
    if search_ids:
        artists = await Artist.select_list(search_ids)
        # try:
        #    artists.sort(
        #        key=lambda r:  (await DateTable.select_id(r.date_id)).date)
        # except AttributeError:
        #    pass
        for artist in artists:
            datetable = await DateTable.select_list([artist.date_id])
            datetable_date = str(datetable[0].date.strftime("%m/%d|%H:%M"))
            cache_dict = {'artists':
                              {'{}'.format(artist.id):
                                   {'name': str(artist.name),
                                    'info_short': str(artist.info_short),
                                    'info_long': str(artist.info_long),
                                    'url_music_1': str(artist.url_music_1),
                                    'url_music_2': str(artist.url_music_2),
                                    'url_1_text': str(artist.url_1_text),
                                    'url_2_text': str(artist.url_2_text),
                                    'act_info': str(artist.act_info),
                                    'time': datetable_date,
                                    'scene': scene
                                    }}}
            await cache_driver.update_dict('common', cache_dict)

    cache_dict = await cache_driver.get_cache('common')
    markup = types.InlineKeyboardMarkup()

    try:
        artist_ids.sort(key=lambda r: str2datetime(cache_dict.get('artists').get(r).get('time')))
    except AttributeError:
        pass

    for i in artist_ids:
        cache_dict_aux = cache_dict.get('artists').get(str(i))
        str_time = cache_dict_aux['time']
        markup.add(types.InlineKeyboardButton('{}({}) {}'.format(str_time.split('|')[1],
                                              day2week.get(str_time.split('|')[0]),
                                              cache_dict_aux['name']),
                                              callback_data="artist_check:{}".format(i)))
    markup.add(types.InlineKeyboardButton("Назад", callback_data="menu"))
    await bot.send_message(query.message['chat']['id'],
                           "Так доповідаю:\n<strong>{}</strong> {}\n{}".format(scene,
                                                                           time_scenes.
                                                                           get(scene).
                                                                           get('str'),
                                                                           scene_info.get(scene)),
                           reply_markup=markup, parse_mode='html')


@dp.callback_query_handler(filters.Regexp(regexp='(stage)'))
async def stage_callback_handler(query: types.CallbackQuery):
    """"""
    await check_in_database(query.message['chat']['id'], query.from_user.id)
    user_message = Message('telegram', query.data, 'stage', '0.01', query.message['chat']['id'],
                           'user')
    await chatbase_sender.send_message(user_message)
    markup = types.InlineKeyboardMarkup()
    dancefloors = await Dancefloor.select_all()
    if dancefloors:
        for dancerfloor in dancefloors:
            markup.add(types.InlineKeyboardButton(dancerfloor.name,
                                                  callback_data="stage_check:{}|{}".format(
                                                      dancerfloor.id, dancerfloor.name)))
        await bot.send_message(query.message['chat']['id'],
                               "Обирай сцену, на якій будеш горіти:".format(), reply_markup=markup, parse_mode='html')
    else:
        markup.add(types.InlineKeyboardButton("Меню",
                                              callback_data="menu"))
        await bot.send_message(query.message['chat']['id'],
                               "Сцен немає.".format(),
                               reply_markup=markup, parse_mode='html')


@dp.callback_query_handler(filters.Regexp(regexp='(set_push:)'))
async def stage_callback_handler(query: types.CallbackQuery):
    """"""
    await check_in_database(query.message['chat']['id'], query.from_user.id)
    user_message = Message('telegram', query.data, 'set_push', '0.01', query.message['chat']['id'],
                           'user')
    await chatbase_sender.send_message(user_message)
    _, id_ = query.data.split(':')

    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Меню",
                                          callback_data="menu"))
    cache_dict = await cache_driver.get_cache('common')
    cache_dict = cache_dict.get('artists').get(id_)
    if not cache_dict:
        artist = await Artist.select(id=id_)
        datetable = await DateTable.select_list([artist.date_id])
        if datetable:
            scene = await Dancefloor.select(datetable[0].dancefloor_id)
            scene = scene.name
            datetable_date = str(datetable[0].date.strftime("%m/%d|%H:%M"))
        else:
            datetable_date, scene = None, None

        cache_dict = {'artists':
                          {'{}'.format(artist.id):
                               {'name': str(artist.name),
                                'info_short': str(artist.info_short),
                                'info_long': str(artist.info_long),
                                'url_music_1': str(artist.url_music_1),
                                'url_music_2': str(artist.url_music_2),
                                'url_1_text': str(artist.url_1_text),
                                'url_2_text': str(artist.url_2_text),
                                'act_info': str(artist.act_info),
                                'time': datetable_date,
                                'scene': scene
                                }}}
        await cache_driver.update_dict('common', cache_dict)
        cache_dict = cache_dict.get('artists').get(id_)

    if str2datetime(cache_dict.get('time')) < datetime.now():
        message = "Нажаль, не можу, <i>{}</i> вже почав у {} на <strong>{}</strong>".format(
            cache_dict.get('name'), cache_dict.get('time').split('|')[1], cache_dict.get('scene'))
    else:
        message = "👍Записав. Кину пуш, коли {} стане за пульт ({} {})".format(
            cache_dict.get('name'), cache_dict.get('time').split('|')[1], cache_dict.get('scene'))
        if (str2datetime(cache_dict.get('time')) - datetime.now()).total_seconds() > 600:
            #print('database')
            await Notification.insert({'chat_id_raw': query.message['chat']['id'],
                                       'date_raw': str2datetime(cache_dict.get('time')),
                                       'scene_raw': cache_dict.get('scene'),
                                       'artist_raw': cache_dict.get('name')
                                        })
        else:
            #print((str2datetime(cache_dict.get('time')) - datetime.now()).total_seconds())
            #print('direct')
            asyncio.create_task(send_notification(((str2datetime(cache_dict.get('time'))
                                      - datetime.now()).total_seconds()), query.message['chat']['id'],
                                                  cache_dict.get('name'), cache_dict.get('scene')))
    await bot.send_message(query.message['chat']['id'],
                           message,
                           reply_markup=markup,
                           parse_mode='html')


@dp.callback_query_handler(filters.Regexp(regexp='(artist_check:)'))
async def artist_dedicated_callback_handler(query: types.CallbackQuery):
    """"""
    await check_in_database(query.message['chat']['id'], query.from_user.id)
    user_message = Message('telegram', query.data, 'artist_check', '0.01', query.message['chat']['id'], 'user')
    await chatbase_sender.send_message(user_message)
    _, id_ = query.data.split(':')

    cache_dict = await cache_driver.get_cache('common')
    cache_dict = cache_dict.get('artists').get(id_)
    if not cache_dict:
        artist = await Artist.select(id=id_)
        datetable = await DateTable.select_list([artist.date_id])
        if datetable:
            scene = await Dancefloor.select(datetable[0].dancefloor_id)
            scene = scene.name
            datetable_date = str(datetable[0].date.strftime("%m/%d|%H:%M"))
        else:
            datetable_date, scene = None, None

        cache_dict = {'artists':
                          {'{}'.format(artist.id):
                               {'name': str(artist.name),
                                'info_short': str(artist.info_short),
                                'info_long': str(artist.info_long),
                                'url_music_1': str(artist.url_music_1),
                                'url_music_2': str(artist.url_music_2),
                                'url_1_text': str(artist.url_1_text),
                                'url_2_text': str(artist.url_2_text),
                                'act_info': str(artist.act_info),
                                'time': datetable_date,
                                'scene': scene
                                }}}
        #for i, j in cache_dict['artists']['{}'.format(artist.id)].items():
            #print(i, type(j))
        await cache_driver.update_dict('common', cache_dict)

        cache_dict = cache_dict.get('artists').get(id_)

    if cache_dict.get('time'):
        message = "<strong>{}</strong> @ {} {} у {}\n".format(cache_dict.get('name'),
                                                         cache_dict.get('scene'),
                                                         cache_dict.get('time').split('|')[1],
                                                         day2week.get(cache_dict.get('time').split('|')[0]))
        if cache_dict.get('info_short') != 'None':
            message += "{}\n".format(cache_dict.get('info_short'))
        if cache_dict.get('act_info') != 'None':
            message += "<i>Виступ: {}</i>\n".format(cache_dict.get('act_info'))
    else:
        """
        message = "<strong>{}</strong>\n{}\n<i>Виступ: {}</i>\n".format(cache_dict.get('name'),
                                                                   cache_dict.get('info_short'),
                                                                   cache_dict.get('act_info'))
        """
        message = "<strong>{}</strong>\n".format(cache_dict.get('name'))
        if cache_dict.get('info_short') != 'None':
            message += "{}\n".format(cache_dict.get('info_short'))
        if cache_dict.get('act_info') != 'None':
            message += "<i>Виступ: {}</i>\n".format(cache_dict.get('act_info'))


    # @TODO MESSAGE
    markup = types.InlineKeyboardMarkup()
    if cache_dict.get('info_long') != 'None':
        markup.add(types.InlineKeyboardButton("Більше інфо", callback_data="artist_more:{}".format(id_)))
    if cache_dict.get('url_music_1') != 'None':
        markup.add(types.InlineKeyboardButton("Послухати", callback_data="artist_music:{}".format(id_)))
    if cache_dict.get('time') != 'None':
        markup.add(types.InlineKeyboardButton("Маякни, коли грає", callback_data="set_push:{}".format(id_)))
    markup.add(types.InlineKeyboardButton("Назад", callback_data="menu"))
    await bot.send_message(query.message['chat']['id'],
                           message,
                           reply_markup=markup,
                           parse_mode='html')


@dp.callback_query_handler(filters.Regexp(regexp='(artist_more:)'))
async def artist_more_callback_handler(query: types.CallbackQuery):
    """"""
    await check_in_database(query.message['chat']['id'], query.from_user.id)
    user_message = Message('telegram', query.data, 'artist_more', '0.01', query.message['chat']['id'], 'user')
    await chatbase_sender.send_message(user_message)
    _, id_ = query.data.split(':')
    cache_dict = await cache_driver.get_cache('common')
    cache_dict = cache_dict.get('artists').get(id_)
    if not cache_dict:
        artist = await Artist.select(id=id_)
        datetable = await DateTable.select_list([artist.date_id])
        if datetable:
            scene = await Dancefloor.select(datetable[0].dancefloor_id)
            datetable_date = datetable.date
        else:
            datetable_date, scene = None, None

        cache_dict = {'artists':
                          {'{}'.format(artist.id):
                               {'name': str(artist.name),
                                'info_short': str(artist.info_short),
                                'info_long': str(artist.info_long),
                                'url_music_1': str(artist.url_music_1),
                                'url_music_2': str(artist.url_music_2),
                                'url_1_text': str(artist.url_1_text),
                                'url_2_text': str(artist.url_2_text),
                                'act_info': str(artist.act_info),
                                'time': datetable_date,
                                'scene': scene
                                }}}
        await cache_driver.update_dict('common', cache_dict)
        cache_dict = cache_dict.get('artists').get(id_)

    message = "<strong>{}</strong>\n{}\n".format(cache_dict.get('name'),
                                                                   cache_dict.get('info_long'),
                                                                   cache_dict.get('act_info'))
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Назад", callback_data="artist_check:{}".format(id_)))
    markup.add(types.InlineKeyboardButton("Повернутися до меню", callback_data="menu"))
    await bot.send_message(query.message['chat']['id'],
                           message,
                           reply_markup=markup,
                           parse_mode='html')


@dp.callback_query_handler(filters.Regexp(regexp='(artist_music:)'))
async def artist_music_callback_handler(query: types.CallbackQuery):
    """"""
    await check_in_database(query.message['chat']['id'], query.from_user.id)
    user_message = Message('telegram', query.data, 'artist_music', '0.01',
                           query.message['chat']['id'], 'user')
    await chatbase_sender.send_message(user_message)
    _, id_ = query.data.split(':')

    cache_dict = await cache_driver.get_cache('common')
    cache_dict = cache_dict.get('artists').get(id_)
    if not cache_dict:
        artist = await Artist.select(id=id_)
        datetable = await DateTable.select_list([artist.date_id])
        if datetable:
            scene = await Dancefloor.select(datetable[0].dancefloor_id)
            datetable_date = datetable.date
        else:
            datetable_date, scene = None, None

        cache_dict = {'artists':
                          {'{}'.format(artist.id):
                               {'name': str(artist.name),
                                'info_short': str(artist.info_short),
                                'info_long': str(artist.info_long),
                                'url_music_1': str(artist.url_music_1),
                                'url_music_2': str(artist.url_music_2),
                                'url_1_text': str(artist.url_1_text),
                                'url_2_text': str(artist.url_2_text),
                                'act_info': str(artist.act_info),
                                'time': datetable_date,
                                'scene': scene
                                }}}
        await cache_driver.update_dict('common', cache_dict)
        cache_dict = cache_dict.get('artists').get(id_)

    if cache_dict.get('url_music_1') and cache_dict.get('url_music_1') != 'None':
        message = '<a href="{}">{}</a>\n'.format(cache_dict.get('url_music_1'), cache_dict.get('url_1_text'))
    if cache_dict.get('url_music_2') and cache_dict.get('url_music_2') != 'None':
        message += '<a href="{}">{}</a>'.format(cache_dict.get('url_music_2'), cache_dict.get('url_2_text'))
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Назад", callback_data="artist_check:{}".format(id_)))
    markup.add(types.InlineKeyboardButton("Повернутися до меню", callback_data="menu"))
    await bot.send_message(query.message['chat']['id'],
                           message,
                           reply_markup=markup,
                           parse_mode='html')


@dp.callback_query_handler(filters.Regexp(regexp='(artist)'))
async def artist_callback_handler(query: types.CallbackQuery):
    """"""
    await check_in_database(query.message['chat']['id'], query.from_user.id)
    user_message = Message('telegram', query.data, 'artist', '0.01', query.message['chat']['id'],
                           'user')
    await chatbase_sender.send_message(user_message)
    await cache_driver.update_dict(query.message['chat']['id'], {'user_status': {'prompt': True}})
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Назад", callback_data="info"))
    await bot.send_message(query.message['chat']['id'],
                           "Напиши нік артиста - а я вгадаю з трьох символів⬇️",
                           reply_markup=markup)

@dp.callback_query_handler(filters.Regexp(regexp='(push_cancel)'))
async def artist_callback_handler(query: types.CallbackQuery):
    """"""
    await check_in_database(query.message['chat']['id'], query.from_user.id)
    await User.update(query.message['chat']['id'], {'is_push_forbidden': True})
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Увімкнути", callback_data="push_on"))
    await bot.send_message(query.message['chat']['id'],
                           "Відключив пуш", reply_markup=markup)


@dp.callback_query_handler(filters.Regexp(regexp='(push_on)'))
async def artist_callback_handler(query: types.CallbackQuery):
    """"""
    await check_in_database(query.message['chat']['id'], query.from_user.id)
    await User.update(query.message['chat']['id'], {'is_push_forbidden': False})
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Відключити", callback_data="push_cancel"))
    await bot.send_message(query.message['chat']['id'],
                           "Увімкнув пуш", reply_markup=markup)


@dp.message_handler()
async def any_message_handler(message: types.Message):
    await check_in_database(message.chat.id, message.from_user.id)
    markup = types.InlineKeyboardMarkup()
    cache_dict = await cache_driver.get_cache(message.chat.id)

    if 'prompt' in cache_dict['user_status'].keys():
        if cache_dict['user_status']['prompt']:
            user_message = Message('telegram', message.text, 'prompt', '0.01', message.from_user.id,
                                   'user')
            await chatbase_sender.send_message(user_message)
            if len(message.text) >= 3:
                artists = await Artist.select(contains=message.text)
                if artists:
                    await cache_driver.update_dict(message.chat.id,
                                                   {'user_status': {'prompt': False}})
                    for artist in artists:
                        #print(artist.name)
                        markup.add(types.InlineKeyboardButton(artist.name,
                                                              callback_data="artist_check:{}".
                                                              format(artist.id)))
                        datetable = await DateTable.select_list([artist.date_id])
                        if datetable:
                            scene = await Dancefloor.select(datetable[0].dancefloor_id)
                            scene = scene.name
                            datetable_date = str(datetable[0].date.strftime("%m/%d|%H:%M"))
                        else:
                            datetable_date, scene = None, None

                        cache_dict = {'artists':
                                          {'{}'.format(artist.id):
                                               {'name': str(artist.name),
                                                'info_short': str(artist.info_short),
                                                'info_long': str(artist.info_long),
                                                'url_music_1': str(artist.url_music_1),
                                                'url_music_2': str(artist.url_music_2),
                                                'url_1_text': str(artist.url_1_text),
                                                'url_2_text': str(artist.url_2_text),
                                                'act_info': str(artist.act_info),
                                                'time': datetable_date,
                                                'scene': scene
                                                }}}
                        await cache_driver.update_dict('common', cache_dict)

                    markup.add(types.InlineKeyboardButton("Меню", callback_data="menu"))
                    await bot.send_message(message.chat.id,
                                           'Знайшов наступних артистів:',
                                           reply_markup=markup)
                else:
                    await cache_driver.update_dict(message.chat.id,
                                                   {'user_status': {'prompt': False}})
                    markup.add(types.InlineKeyboardButton("Меню", callback_data="menu"))
                    await bot.send_message(message.chat.id, 'Вибач, я не знаю {}. Спробуй пошукати за сценою.'.
                                           format(message.text),
                                           reply_markup=markup)
            else:
                markup.add(types.InlineKeyboardButton("Меню", callback_data="menu"))
                await bot.send_message(message.chat.id, 'Три символи — я не вгадаю, якщо менше.',
                                       reply_markup=markup)

        else:
            user_message = Message('telegram', message.text, 'start', '0.01', message.from_user.id,
                                   'user', True)
            await chatbase_sender.send_message(user_message)
            markup.add(types.InlineKeyboardButton("Меню", callback_data="menu"))
            await bot.send_message(message.chat.id, 'Ой нє, такого мене ще не навчили.',
                                   reply_markup=markup)
    else:
        user_message = Message('telegram', message.text, 'start', '0.01', message.from_user.id,
                               'user', True)
        await chatbase_sender.send_message(user_message)
        markup.add(types.InlineKeyboardButton("Меню", callback_data="menu"))

        await bot.send_message(message.chat.id, 'Ой нє, такого мене ще не навчили.',
                               reply_markup=markup)
        """
        await bot.send_message(message.chat.id, 'Дядя, збираю кабіну. Почитай поки про лайнап Brave на '
                                                '<a href="https://katacult.com/brave-factory-festival/">Katacult</a>',
                               parse_mode='html')
        """


async def cache_check():
    logging.info('Cache check enable')
    while True:
        cache_dict = await cache_driver.get_cache('cache_check')
        if cache_dict.get('check'):
            await cache_driver.set_cache('common', {'artists': {}})
            await cache_driver.set_cache('lineup', {'info': {}})
            await cache_driver.clear_cache('cache_check', 'redis_alt')
            #await cache_driver.clear_cache('cache_check', 'default')

        await asyncio.sleep(1 * 60)


async def push_check():
    logging.info('Push check enable')
    while True:
        logging.info('Push check...')
        push_dict = await cache_driver.get_cache('push')
        if push_dict.get('push_message'):
            if push_dict.get('push_message').get('to_send'):
                logging.info('Push received, start broadcasting')
                await cache_driver.clear_cache('push', 'redis_alt')
                #await cache_driver.clear_cache('push', 'default')
                users = await User.select_all()
                for user in users:
                    if not user.is_push_forbidden:
                        if push_dict.get('push_message').get('type') == 'text':
                            markup = types.InlineKeyboardMarkup()
                            markup.add(types.InlineKeyboardButton("Відключити пуш-повідомлення", callback_data="push_cancel"))
                            try:
                                await bot.send_message(int(user.tg_chat_id),
                                                       push_dict.get('push_message').get('message'),
                                                       reply_markup=markup)
                            except:
                                logging.info('не зміг відправити пуша юзеру {}. мабуть заблочив'.format(user.tg_chat_id))
                                pass
                        elif push_dict.get('push_message').get('type') == 'animation':
                            markup = types.InlineKeyboardMarkup()
                            markup.add(types.InlineKeyboardButton("Відключити пуш-повідомлення", callback_data="push_cancel"))
                            #animation = types.InputMediaAnimation(push_dict.get('push_message').get('message'))
                            await bot.send_animation(chat_id=int(user.tg_chat_id),
                                                     animation=push_dict.get('push_message').get('message'),
                                                     reply_markup=markup)
                        await asyncio.sleep(0.06)
        await asyncio.sleep(1 * 60)


async def notification_check():
    logging.info('Notification check enable')
    while True:
        logging.info('Check notification:')
        result = await Notification.select_all()
        for i in result:
            aux_time = (i.date_raw - datetime.now()).total_seconds()
            if aux_time < 300:
                asyncio.create_task(send_notification(int(aux_time), i.chat_id_raw, i.artist_raw,
                                                      i.scene_raw))
                await Notification.delete(id=i.id)
                # await asyncio.create_task(send_notification(int(aux_time), i.chat_id_raw, i.artist_raw,
                #                             LjL         i.scene_raw))
        await asyncio.sleep(5 * 60)


async def send_notification(timeleft, chat_id, name, scene):
    logging.info('Spawn notification:')
    if timeleft > 4:
        await asyncio.sleep((timeleft - 4)*60)
    await bot.send_message(chat_id, "<i>{}</i> уже за пультом на сцені <strong>{}</strong>. Бігом!".format(
            name, scene), parse_mode='html')


async def create_scene_helper():
    dancefloors = await Dancefloor.select_all()
    dict_ = {}
    for i in dancefloors:
        dict_.update({str(i.id):str(i.name)})
        dict_.update({str(i.name): str(i.id)})
    return dict_

async def on_startup(dp):
    cache_driver.cache_create('redis_alt')
    #cache_driver.cache_create('default')
    await cache_driver.set_cache('common', {'artists': {}})
    await cache_driver.set_cache('lineup', {'info': {}})
    aux_scenes = await create_scene_helper()
    await cache_driver.set_cache('scenes', aux_scenes)
    await bot.set_webhook(WEBHOOK_URL)
    asyncio.create_task(notification_check())
    asyncio.create_task(push_check())
    asyncio.create_task(cache_check())



async def on_shutdown(dp):
    pass


if __name__ == '__main__':
    start_webhook(dispatcher=dp, webhook_path=WEBHOOK_PATH, on_startup=on_startup, on_shutdown=on_shutdown,
                  skip_updates=True, host=WEBAPP_HOST, port=WEBAPP_PORT)
