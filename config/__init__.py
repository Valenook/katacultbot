import os
import yaml


stage = os.environ.get("BUS_BOT_STAGE", "dev")
filename = os.path.join(os.path.dirname(__file__), "config.yaml")

with open(filename) as f:
    config = yaml.safe_load(f)
    config = config[stage]
